package observers;

public interface Observer 
{
	public void update(int acc,float f);
}
