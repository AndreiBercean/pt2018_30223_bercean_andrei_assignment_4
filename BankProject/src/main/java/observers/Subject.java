package observers;
import core.Person;

public interface Subject 
{
	public void addObserver(Person p);
	public void removeObserver(Person p);
	public void notifyObserver(float f);
}
