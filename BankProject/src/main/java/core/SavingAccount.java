package core;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("serial")
public class SavingAccount extends Account 
{
	long timeLastUsed = 0;
	
	public SavingAccount(int n){
		number = n;		
	}

	public boolean deposit(float ammount) {
		sum += ammount;
		this.notifyObserver(ammount);
		return true;
	}

	public boolean withdraw(float ammount) {
		long timeMillis = System.currentTimeMillis();
		long timeSeconds = TimeUnit.MILLISECONDS.toSeconds(timeMillis);
		if((timeSeconds - timeLastUsed) > 5)
		{
			timeLastUsed = timeSeconds;
			sum -= ammount;
			this.notifyObserver(-ammount);
			return true;
		}
		else 
		{
			System.out.println("Need to wait " + (6 - (timeSeconds - timeLastUsed))+" second(s)");
			return false;
		}
	}

	public float getSum() {
		return sum;
	}
	
	public int getNumber() {
		return number;
	}

	public String toString() {
		return "Saving account: " + number + "  sum: " + sum;
	}

	public boolean equals(Object obj) 
	{
		if (obj == null || obj.getClass() != this.getClass()) 
		{ 
			return false; 
		} 
		SavingAccount aux = (SavingAccount) obj;
		return number == aux.getNumber();
	}
	
	public void addObserver(Person p) 
	{
		observerList.add(p);
	}

	public void removeObserver(Person p) 
	{
		observerList.remove(p);
	}

	public void notifyObserver(float f) 
	{
		for(Person p : observerList)
		{
			p.update(this.getNumber(), f);
		}
	}

	public String[] toStringVector() 
	{
		String[] data = {""+observerList.get(0).getCNP(), ""+number, "Saving", ""+sum};
		return data;
	}
}
