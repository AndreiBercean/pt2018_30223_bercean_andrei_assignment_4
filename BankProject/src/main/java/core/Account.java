package core;

import java.io.*;
import java.util.*;
import observers.*;

@SuppressWarnings("serial")
public abstract class Account implements Serializable,Subject
{
	protected float sum = 0;
	
	protected int number;
	
	protected ArrayList<Person> observerList = new ArrayList<Person>();
	
	abstract boolean deposit (float ammount);
	
	abstract boolean withdraw (float ammount);
	
	abstract float getSum ();
	
	abstract int getNumber ();
	
	public abstract boolean equals(Object obj);
	
	abstract public String toString();
	
	abstract public String[] toStringVector();
}
