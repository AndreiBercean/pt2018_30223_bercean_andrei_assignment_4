package core;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;

public class GUI 
{
	
	static Bank b = new Bank();
	
	static void populate(DefaultTableModel model, ArrayList<String[]> rawData)
	{
		for(String[] s : rawData)
		{
			model.addRow(s);
		}
	}
	
	public static void main(String[] args) 
	{
		final Color MAROON = new Color(128, 0, 0);
		final JFrame bank = new JFrame("Bank");
		
		final JFrame people = new JFrame("People");
		people.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		final JFrame account = new JFrame("Account");
		account.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		account.setSize(300,200);
		
		JPanel controlPane = new JPanel();
		JPanel mainPane = new JPanel();
		bank.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		bank.setSize(600,500);
		b.load();
		
		mainPane.setLayout(new GridLayout());
		mainPane.setBackground(Color.LIGHT_GRAY);
		mainPane.setForeground(Color.BLACK);
		
		
		//-=Tabel=-//
		DefaultTableModel personModelInit = new DefaultTableModel();
		personModelInit.addColumn("CNP");personModelInit.addColumn("Name");personModelInit.addColumn("Address");personModelInit.addColumn("Phone number");
		populate(personModelInit,b.getPersonData());
		
		DefaultTableModel accountModelInit = new DefaultTableModel();
		accountModelInit.addColumn("CNP");accountModelInit.addColumn("ID");accountModelInit.addColumn("Type");accountModelInit.addColumn("SUM");
		populate(accountModelInit,b.getAccountData());
		
		final JTable table = new JTable(accountModelInit);
		JScrollPane tabelPane = new JScrollPane(table);
		table.setFillsViewportHeight(true);
		mainPane.add(tabelPane);
		//------//
		
		//-=Control=-//
		controlPane.setLayout(null);
		controlPane.setBackground(Color.LIGHT_GRAY);
		controlPane.setForeground(Color.BLACK);
		
		JButton seePersonsButton = new JButton("People");
		seePersonsButton.setBounds(30,10,100,35);
		seePersonsButton.setBackground(Color.DARK_GRAY);
		seePersonsButton.setForeground(Color.WHITE);
		seePersonsButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				DefaultTableModel personModelNew = new DefaultTableModel();
				personModelNew.addColumn("CNP");personModelNew.addColumn("Name");personModelNew.addColumn("Address");personModelNew.addColumn("Phone number");
				populate(personModelNew,b.getPersonData());
				table.setModel(personModelNew);
				table.repaint();
			}
		});
		controlPane.add(seePersonsButton);
		
		JButton personButton = new JButton("Manage People");
		personButton.setBounds(70,160,150,35);
		personButton.setBackground(MAROON);
		personButton.setForeground(Color.WHITE);
		personButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				//ADD//
				people.setSize(500,300);
				people.setLayout(null);
				JLabel ADDLabel = new JLabel("ADD");
				ADDLabel.setBounds(10, 10, 90, 20);
				people.add(ADDLabel);
				
				JLabel CNPLabel = new JLabel("CNP");
				CNPLabel.setBounds(10, 40, 30, 30);
				people.add(CNPLabel);
				final JTextField CNPBox  = new JTextField();
				CNPBox.setBounds(10,70,30,30);
				CNPBox.setBackground(Color.WHITE);
				CNPBox.setForeground(Color.BLACK);
				people.add(CNPBox);
				
				JLabel NameLabel = new JLabel("Name");
				NameLabel.setBounds(60, 40, 50, 30);
				people.add(NameLabel);
				final JTextField NameBox  = new JTextField();
				NameBox.setBounds(60,70,50,30);
				NameBox.setBackground(Color.WHITE);
				NameBox.setForeground(Color.BLACK);
				people.add(NameBox);
				
				JLabel AddrLabel = new JLabel("Address");
				AddrLabel.setBounds(120, 40, 50, 30);
				people.add(AddrLabel);
				final JTextField AddrBox  = new JTextField();
				AddrBox.setBounds(120,70,50,30);
				AddrBox.setBackground(Color.WHITE);
				AddrBox.setForeground(Color.BLACK);
				people.add(AddrBox);
				
				JLabel PhoneLabel = new JLabel("Phone");
				PhoneLabel.setBounds(180, 40, 50, 30);
				people.add(PhoneLabel);
				final JTextField PhoneBox  = new JTextField();
				PhoneBox.setBounds(180,70,50,30);
				PhoneBox.setBackground(Color.WHITE);
				PhoneBox.setForeground(Color.BLACK);
				people.add(PhoneBox);
				
				JButton addPerson = new JButton("ADD");
				addPerson.setBounds(250,70,100,30);
				addPerson.setBackground(MAROON);
				addPerson.setForeground(Color.WHITE);
				addPerson.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						b.addPerson(new Person(Integer.parseInt(CNPBox.getText()),NameBox.getText(),AddrBox.getText(),PhoneBox.getText()));
						DefaultTableModel personModelNew = new DefaultTableModel();
						personModelNew.addColumn("CNP");personModelNew.addColumn("Name");personModelNew.addColumn("Address");personModelNew.addColumn("Phone number");
						populate(personModelNew,b.getPersonData());
						table.setModel(personModelNew);
						table.repaint();
					}
				});
				people.add(addPerson);
				
				//EDIT//
				JLabel CNPLabelE = new JLabel("CNP");
				CNPLabelE.setBounds(10, 90, 30, 30);
				people.add(CNPLabelE);
				final JTextField CNPBoxE  = new JTextField();
				CNPBoxE.setBounds(10,120,30,30);
				CNPBoxE.setBackground(Color.WHITE);
				CNPBoxE.setForeground(Color.BLACK);
				people.add(CNPBoxE);
				
				JLabel NameLabelE = new JLabel("Name");
				NameLabelE.setBounds(60, 90, 50, 30);
				people.add(NameLabelE);
				final JTextField NameBoxE  = new JTextField();
				NameBoxE.setBounds(60,120,50,30);
				NameBoxE.setBackground(Color.WHITE);
				NameBoxE.setForeground(Color.BLACK);
				people.add(NameBoxE);
				
				JLabel AddrLabelE = new JLabel("Address");
				AddrLabelE.setBounds(120, 90, 50, 30);
				people.add(AddrLabelE);
				final JTextField AddrBoxE  = new JTextField();
				AddrBoxE.setBounds(120,120,50,30);
				AddrBoxE.setBackground(Color.WHITE);
				AddrBoxE.setForeground(Color.BLACK);
				people.add(AddrBoxE);
				
				JLabel PhoneLabelE = new JLabel("Phone");
				PhoneLabelE.setBounds(180, 90, 50, 30);
				people.add(PhoneLabelE);
				final JTextField PhoneBoxE  = new JTextField();
				PhoneBoxE.setBounds(180,120,50,30);
				PhoneBoxE.setBackground(Color.WHITE);
				PhoneBoxE.setForeground(Color.BLACK);
				people.add(PhoneBoxE);
				
				JButton editPerson = new JButton("EDIT");
				editPerson.setBounds(250,120,100,30);
				editPerson.setBackground(MAROON);
				editPerson.setForeground(Color.WHITE);
				editPerson.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						b.editPerson(new Person(Integer.parseInt(CNPBoxE.getText()),NameBoxE.getText(),AddrBoxE.getText(),PhoneBoxE.getText()));
						DefaultTableModel personModel = new DefaultTableModel();
						personModel.addColumn("CNP");personModel.addColumn("Name");personModel.addColumn("Address");personModel.addColumn("Phone number");
						populate(personModel,b.getPersonData());
						table.setModel(personModel);
						table.repaint();
					}
				});
				people.add(editPerson);
				
				//DELETE//
				JLabel CNPLabelD = new JLabel("CNP");
				CNPLabelD.setBounds(10, 140, 30, 30);
				people.add(CNPLabelD);
				final JTextField CNPBoxD  = new JTextField();
				CNPBoxD.setBounds(10,180,30,30);
				CNPBoxD.setBackground(Color.WHITE);
				CNPBoxD.setForeground(Color.BLACK);
				people.add(CNPBoxD);
				
				JButton deletePerson = new JButton("DELETE");
				deletePerson.setBounds(60,180,100,30);
				deletePerson.setBackground(MAROON);
				deletePerson.setForeground(Color.WHITE);
				deletePerson.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						b.deletePerson(new Person(Integer.parseInt(CNPBoxD.getText()),"","",""));
						DefaultTableModel personModel = new DefaultTableModel();
						personModel.addColumn("CNP");personModel.addColumn("Name");personModel.addColumn("Address");personModel.addColumn("Phone number");
						populate(personModel,b.getPersonData());
						table.setModel(personModel);
						table.repaint();
					}
				});
				people.add(deletePerson);
				//------//
				
				people.setResizable(false);
				people.setVisible(true);
				account.dispose();
			}
		});
		controlPane.add(personButton);
		
		JButton accountButton = new JButton("Manage Accounts");
		accountButton.setBounds(70,200,150,35);
		accountButton.setBackground(MAROON);
		accountButton.setForeground(Color.WHITE);
		accountButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				
				//ADD//
				account.setSize(500,300);
				account.setLayout(null);
				JLabel ADDLabel = new JLabel("ADD");
				ADDLabel.setBounds(10, 10, 90, 20);
				account.add(ADDLabel);
				
				JLabel CNPLabel = new JLabel("CNP");
				CNPLabel.setBounds(10, 40, 30, 30);
				account.add(CNPLabel);
				final JTextField CNPBox  = new JTextField();
				CNPBox.setBounds(10,70,30,30);
				CNPBox.setBackground(Color.WHITE);
				CNPBox.setForeground(Color.BLACK);
				account.add(CNPBox);
				
				JLabel NameLabel = new JLabel("Type");
				NameLabel.setBounds(60, 40, 50, 30);
				account.add(NameLabel);
				String[] types = {"Saving","Spendng"};
				final JComboBox typeBox = new JComboBox(types);
				typeBox.setEditable(false);
				typeBox.setBounds(60,70,80,30);
				typeBox.setBackground(Color.WHITE);
				typeBox.setForeground(Color.BLACK);
				account.add(typeBox);
				
				JButton addAccount = new JButton("ADD");
				addAccount.setBounds(160,70,100,30);
				addAccount.setBackground(MAROON);
				addAccount.setForeground(Color.WHITE);
				addAccount.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						String x = typeBox.getSelectedItem().toString();
						int type;
						if(x == "Saving")type = 0;
						else type = 1;
						b.addAccount(new Person(Integer.parseInt(CNPBox.getText()), "","",""),type);
						DefaultTableModel accountModel = new DefaultTableModel();
						accountModel.addColumn("CNP");accountModel.addColumn("ID");accountModel.addColumn("Type");accountModel.addColumn("SUM");
						populate(accountModel,b.getAccountData());
						table.setModel(accountModel);
						table.repaint();
					}
				});
				account.add(addAccount);
				
				//EDIT//
				JLabel CNPLabelE = new JLabel("CNP");
				CNPLabelE.setBounds(10, 90, 30, 30);
				account.add(CNPLabelE);
				final JTextField CNPBoxE  = new JTextField();
				CNPBoxE.setBounds(10,120,30,30);
				CNPBoxE.setBackground(Color.WHITE);
				CNPBoxE.setForeground(Color.BLACK);
				account.add(CNPBoxE);
				
				JLabel IDLabelE = new JLabel("ID");
				IDLabelE.setBounds(60, 90, 50, 30);
				account.add(IDLabelE);
				final JTextField IDBoxE  = new JTextField();
				IDBoxE.setBounds(60,120,50,30);
				IDBoxE.setBackground(Color.WHITE);
				IDBoxE.setForeground(Color.BLACK);
				account.add(IDBoxE);
				
				JLabel SumLabelE = new JLabel("SUM");
				SumLabelE.setBounds(120, 90, 50, 30);
				account.add(SumLabelE);
				final JTextField SumBoxE  = new JTextField();
				SumBoxE.setBounds(120,120,50,30);
				SumBoxE.setBackground(Color.WHITE);
				SumBoxE.setForeground(Color.BLACK);
				account.add(SumBoxE);
				
				JButton depositAccount = new JButton("DEPOSIT");
				depositAccount.setBounds(180, 120,100,30);
				depositAccount.setBackground(MAROON);
				depositAccount.setForeground(Color.WHITE);
				depositAccount.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						b.editAccount(new Person(Integer.parseInt(CNPBoxE.getText()),"","",""), Integer.parseInt(IDBoxE.getText()), 0, Float.parseFloat(SumBoxE.getText()));
						DefaultTableModel accountModel = new DefaultTableModel();
						accountModel.addColumn("CNP");accountModel.addColumn("ID");accountModel.addColumn("Type");accountModel.addColumn("SUM");
						populate(accountModel,b.getAccountData());
						table.setModel(accountModel);
						table.repaint();
					}
				});
				account.add(depositAccount);
				
				JButton withdrawAccount = new JButton("WITHDRAW");
				withdrawAccount.setBounds(290,120,100,30);
				withdrawAccount.setBackground(MAROON);
				withdrawAccount.setForeground(Color.WHITE);
				withdrawAccount.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						b.editAccount(new Person(Integer.parseInt(CNPBoxE.getText()),"","",""), Integer.parseInt(IDBoxE.getText()), 1, Float.parseFloat(SumBoxE.getText()));
						DefaultTableModel accountModel = new DefaultTableModel();
						accountModel.addColumn("CNP");accountModel.addColumn("ID");accountModel.addColumn("Type");accountModel.addColumn("SUM");
						populate(accountModel,b.getAccountData());
						table.setModel(accountModel);
						table.repaint();
					}
				});
				account.add(withdrawAccount);
				
				//DELETE//
				JLabel CNPLabelD = new JLabel("CNP");
				CNPLabelD.setBounds(10, 140, 30, 30);
				account.add(CNPLabelD);
				final JTextField CNPBoxD  = new JTextField();
				CNPBoxD.setBounds(10,175,30,30);
				CNPBoxD.setBackground(Color.WHITE);
				CNPBoxD.setForeground(Color.BLACK);
				account.add(CNPBoxD);
				
				JLabel IDLabelD = new JLabel("ID");
				IDLabelD.setBounds(60, 140, 30, 30);
				account.add(IDLabelD);
				JTextField IDBoxD  = new JTextField();
				IDBoxD.setBounds(60,175,30,30);
				IDBoxD.setBackground(Color.WHITE);
				IDBoxD.setForeground(Color.BLACK);
				account.add(IDBoxD);
				
				JButton deletePerson = new JButton("DELETE");
				deletePerson.setBounds(120,175,100,30);
				deletePerson.setBackground(MAROON);
				deletePerson.setForeground(Color.WHITE);
				deletePerson.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						b.deletePerson(new Person(Integer.parseInt(CNPBoxD.getText()),"","",""));
						DefaultTableModel accountModel = new DefaultTableModel();
						accountModel.addColumn("CNP");accountModel.addColumn("ID");accountModel.addColumn("Type");accountModel.addColumn("SUM");
						populate(accountModel,b.getAccountData());
						table.setModel(accountModel);
						table.repaint();
					}
				});
				account.add(deletePerson);
				//------//
				
				account.setResizable(false);
				account.setVisible(true);
				people.dispose();
			}
		});
		controlPane.add(accountButton);
		
		JButton seeAccountsButton = new JButton("Accounts");
		seeAccountsButton.setBounds(150,10,100,35);
		seeAccountsButton.setBackground(Color.DARK_GRAY);
		seeAccountsButton.setForeground(Color.WHITE);
		seeAccountsButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				DefaultTableModel accountModel = new DefaultTableModel();
				accountModel.addColumn("CNP");accountModel.addColumn("ID");accountModel.addColumn("Type");accountModel.addColumn("SUM");
				populate(accountModel,b.getAccountData());
				table.setModel(accountModel);
				table.repaint();
			}
		});
		controlPane.add(seeAccountsButton);
		
		JButton saveAndExitButton = new JButton("Quit");
		saveAndExitButton.setBounds(100,400,100,35);
		saveAndExitButton.setBackground(MAROON);
		saveAndExitButton.setForeground(Color.WHITE);
		saveAndExitButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				b.store();
				bank.dispose();
				people.dispose();
				account.dispose();
			}
		});
		controlPane.add(saveAndExitButton);
		
		mainPane.add(controlPane);
		//------//
		
		bank.add(mainPane);
		bank.setResizable(false);
		bank.setVisible(true);
	}
}
