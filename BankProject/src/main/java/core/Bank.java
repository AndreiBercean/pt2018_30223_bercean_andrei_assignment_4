package core;

import java.io.*;
import java.util.*;

public class Bank implements BankProc
{	
	HashMap<Person,ArrayList<Account>> hMap = new HashMap<Person,ArrayList<Account>>();
	
	public void addPerson(Person p) 
	{
		assert hMap.containsKey(p) == false : "P_ADD: Person already exist in database";
		ArrayList<Account> aux = new ArrayList<Account>();
		hMap.put(p, aux);
		assert hMap.containsKey(p) == true : "P_ADD: Person doesn't exist in database";
	}

	public void editPerson(Person p) 
	{
		assert hMap.containsKey(p) == true : "P_EDIT: Person doesn't exist in database (before)";
		ArrayList<Account> aux = hMap.get(p);
		hMap.remove(p);
		hMap.put(p, aux);
		assert hMap.containsKey(p) == true : "P_EDIT: Person doesn't exist in database (after)";
	}

	public void deletePerson(Person p) 
	{
		assert hMap.containsKey(p) == true : "P_DEL: Person doesn't exist in database";
		hMap.remove(p);
		assert hMap.containsKey(p) == false : "P_DEL: Person still exist in database";
	}

	public void addAccount(Person p, int type) 
	{
		Account a;
		int nr;
		
		assert hMap.containsKey(p) : "A_ADD: Person not in database";
		if(hMap.containsKey(p))
		{
			ArrayList<Account> aux = hMap.get(p);
			if(aux.size() < 1)nr = p.getCNP()*100;
			else nr = aux.get(aux.size()-1).getNumber();
			
			if(type == 1)a = new SpendingAccount(nr+1);
			else a = new SavingAccount(nr+1);
			
			a.addObserver(p);
			
			aux.add(a);
		}
	}

	public void editAccount(Person p, int id, int op, float sum) 
	{
		assert sum > 0 : "A_EDIT: Sum not positive";
		assert hMap.containsKey(p) == true : "A_EDIT: Person not in database";
		ArrayList<Account> aux = hMap.get(p);
		assert aux.size()>1 : "A_EDIT: Account not in database";
		
		Account a1 = new SavingAccount(id);
		Account a2 = new SpendingAccount(id);
		
		int index = aux.indexOf(a1);
		if(index == -1)index = aux.indexOf(a2);
		
		assert index != -1 : "A_EDIT: Account not in database";
		
		Account acc = aux.get(index);
		
		if(op == 0)
		{
			acc.deposit(sum);
		}
		else
		{
			acc.withdraw(sum);
		}
	}

	public void deleteAccount(Person p, int id) 
	{
		assert hMap.containsKey(p) == true : "A_EDIT: Person not in database";
		ArrayList<Account> aux = hMap.get(p);
		
		Account a1 = new SavingAccount(id);
		Account a2 = new SpendingAccount(id);
		
		int index = aux.indexOf(a1);
		if(index == -1)index = aux.indexOf(a2);
		
		assert index != -1 : "A_EDIT: Account not in database";
		
		aux.remove(index);
	}
	
	public void store()
	{
		try 
		{
			FileOutputStream fileOut = new FileOutputStream("bankInfo.ser");
	        ObjectOutputStream out = new ObjectOutputStream(fileOut);
	        out.writeObject(hMap);
	        out.close();
	        fileOut.close();
	      } 
		catch (IOException i) 
		{
	        i.printStackTrace();
	    }
	}

	@SuppressWarnings("unchecked")
	public void load()
	{
	    try 
	    {
	       FileInputStream fileIn = new FileInputStream("bankInfo.ser");
	       ObjectInputStream in = new ObjectInputStream(fileIn);
	       hMap = (HashMap<Person,ArrayList<Account>>) in.readObject();
	       in.close();
	       fileIn.close();
	    } 
	    catch (IOException i) 
	    {
	       i.printStackTrace();
	    } 
	    catch (ClassNotFoundException c) 
	    {
	       System.out.println("Employee class not found");
	       c.printStackTrace();
	    }
	}
	
	public ArrayList<String[]> getAccountData()
	{
		ArrayList<String[]> data = new ArrayList<String[]>();
		for(Map.Entry me : hMap.entrySet())
		{
			ArrayList<Account> aux =  (ArrayList<Account>) me.getValue();
			if(aux.size()>0)
			{
				for(Account a : aux)
				{
					data.add(a.toStringVector());
				}
			}
			
		}
		return data;
	}
	
	public ArrayList<String[]> getPersonData()
	{
		ArrayList<String[]> data = new ArrayList<String[]>();
		for(Map.Entry me : hMap.entrySet())
		{
			data.add(((Person)me.getKey()).toStringVector());
		}
		return data;
	}
	
	public void print_all()
	{
		for(Map.Entry me : hMap.entrySet())
		{
			ArrayList<Account> aux =  (ArrayList<Account>) me.getValue();
			System.out.println(((Person)me.getKey()).toString() + "\n--Accounts--");
			if(aux.size()<1)System.out.println("None");
			for(Account a : aux )
			{
				System.out.println(a.toString());
			}
			//System.out.println("\n");
		}
	}
}
