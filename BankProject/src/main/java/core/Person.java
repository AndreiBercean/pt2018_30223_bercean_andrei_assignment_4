package core;
import observers.*;
import java.io.*;

@SuppressWarnings("serial")
public class Person implements Serializable,Observer 
{
	private int CNP;
	private String name;
	private String address;
	private String phoneNumber;
	
	public Person(int c,String n,String a,String p)
	{
		CNP = c;
		name = n;
		address = a;
		phoneNumber = p;
	}
	
	public int getCNP()
	{
		return CNP;
	}
	
	public boolean equals(Object obj) 
	{ 
		if (obj == this) 
		{ 
			return true; 
		} 
		
		if (obj == null || obj.getClass() != this.getClass()) 
		{ 
			return false; 
		} 
		
		Person p = (Person) obj; 
		return CNP == p.CNP;	
	}
	
	public int hashCode() 
	{ 
		return CNP; 
	}
	
	public String toString()
	{
		return "CNP: "+CNP+"\nNume: "+name+"\nAdresa: "+address+"\nTelefon: "+phoneNumber;
	}
	
	public String[] toStringVector()
	{
		String[] data = {""+CNP,name,address,phoneNumber};
		return data;
	}
	
	public void update(int acc, float f) {
		System.out.println("Account "+acc+" changed sum with "+f);
	}
}
