package core;

public interface BankProc
{	
	//-=Persons=-//
	public void addPerson(Person p);
	public void editPerson(Person p);
	public void deletePerson(Person p);
	
	//-=Accounts=-//
	public void addAccount(Person p, int type);
	public void editAccount(Person p, int id, int op, float sum);
	public void deleteAccount(Person p, int id);
	
	//-=Mem Store=-//
	public void store();
	public void load();	
}
