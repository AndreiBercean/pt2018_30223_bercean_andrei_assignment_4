package core;

@SuppressWarnings("serial")
public class SpendingAccount extends Account 
{
	public SpendingAccount(int n){
		number = n;
	}

	public boolean deposit(float ammount) {
		sum += ammount;
		this.notifyObserver(ammount);
		return true;
	}

	public boolean withdraw(float ammount) {
		sum -= ammount;
		this.notifyObserver(-ammount);
		return true;
	}

	public float getSum() {
		return sum;
	}

	public int getNumber() {
		return number;
	}

	public String toString() {
		return "Spending account " + number + " sum: " + sum;
	}

	public boolean equals(Object obj) 
	{
		if (obj == null || obj.getClass() != this.getClass()) 
		{ 
			return false; 
		} 
		SpendingAccount aux = (SpendingAccount) obj;
		return number == aux.getNumber();
	}
	
	public void addObserver(Person p) 
	{
		observerList.add(p);
	}

	public void removeObserver(Person p) 
	{
		observerList.remove(p);
	}

	public void notifyObserver(float f) 
	{
		for(Person p : observerList)
		{
			p.update(this.getNumber(), f);
		}
	}

	public String[] toStringVector() 
	{
		String[] data = {""+observerList.get(0).getCNP(), ""+number, "Spending", ""+sum};
		return data;
	}
}
